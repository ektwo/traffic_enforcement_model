# James
import os
import sys
import shutil


def roundup_bigthan1(val, valStr):
    #print('roundup_bigthan1 1 valStr=',valStr)
    tmp = val
    if tmp >= 1:
        return
    else:
        tmp = tmp * 10
        #print('roundup_bigthan1 2 str(float(tmp))=',str(float(tmp)))
        valStr.append(str(int(tmp)))
        #print('roundup_bigthan1 3 valStr=',valStr)
        roundup_bigthan1(tmp, valStr)
    
    
def mkdir(dirPath):
    if sys.version_info[0] < 3 and sys.version_info[1] < 2:
        if not os.path.exists(dirPath):
            os.makedirs(dirPath)  
    else:
        os.makedirs(dirPath, exist_ok=True)     

def mkfiledir(filePath):
    if sys.version_info[0] < 3 and sys.version_info[1] < 2:
        directory = os.path.dirname(filePath)
        if not os.path.exists(directory):
            os.makedirs(directory)  
    else:
        os.makedirs(os.path.dirname(filePath), exist_ok=True)        


def copyfile(src, dst):
    if os.path.isfile(src)==True:
        if sys.version_info[0] < 3.2:
            try:
                shutil.copy2(src, dst)
            except IOError as io_err:
                os.makedirs(os.path.dirname(dst))
                shutil.copy2(src, dst)
        else:
            os.makedirs(os.path.dirname(dst), exist_ok=True)
            shutil.copy2(src, dst)    