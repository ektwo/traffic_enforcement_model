import os
import sys
import cv2


def check_validation(filePath, width, height, channels):
    if os.path.exists(filePath):
        img = cv2.imread(filePath)
        if img is not None:
            height, width, channels = img.shape
            return True
        
    return False

def write_img(img, dstPath, width, height):
    if img is not None:   
        imgResized = cv2.resize(img, (width, height))
        cv2.imwrite(dstPath, imgResized)       