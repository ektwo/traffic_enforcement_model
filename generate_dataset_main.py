import os
import os.path
import sys
import glob
import subprocess
import cv2
from PIL import Image
from xml.dom import minidom
import xml.etree.ElementTree as ET
import kneron_config
import ek_common


#help(cv2)
#
#
#try:
#    user_paths = os.environ['PATH'].split(os.pathsep)
#except:
#    user_paths=[]
#print('PATH=',user_paths)
#
#try:
#    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
#except:
#    user_paths=[]
#print('PYTHONPATH=',user_paths)
#

#print(sys.path)
#
#import pip
#installed_packages=pip.get_installed_distributions()
#installed_packages_list=sorted(["%s==%s" % (i.key, i.version) 
#    for i in installed_packages])
#print(installed_packages_list)

print(os.getcwd())
      
print(cv2.__version__)

#from tensorflow.python.client import device_lib
#print(device_lib.list_local_devices())

#sys.exit(0)

#os.system("python -c \"from distutils.sysconfig import get_python_lib;print(get_python_lib())\"")

#def TestWait():
#  import subprocess
#  import datetime
#  print (datetime.datetime.now())
#  p=subprocess.Popen("python Add_No_car_xml_002.py",shell=True)
#  p.wait()
#  print (p.returncode)
#  print (datetime.datetime.now())

#TestWait()




#import logging
#
#logging.basicConfig(level=logging.INFO)
##logging.basicConfig(filename='logging.txt')
#logging.warning('Hello world!')
#logging.info('Hello world again!') 
#
#def run_it(cmd):
#    # _PIPE = subprocess.PIPE
#    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True,
#                         stderr=subprocess.PIPE) #, close_fds=True)
#
#    logging.debug('running:%s' % cmd)
#    out, err = p.communicate()
#    logging.debug(out)
#    if p.returncode != 0:
#        logging.critical("Non zero exit code:%s executing: %s" % (p.returncode, cmd))
#    return p.stdout





#run_it("python Add_No_car_xml_002.py")

import shutil
# merge

#ORG_DATASET_DIR="org_dataset"

#AUGMENTATION_DATASET_DIR="augmentation_dataset"

root = os.getcwd()
root = os.path.abspath(root)
dstDatasetPath = os.path.join(os.path.dirname(root), kneron_config.IMG_PARENT_NAME)

#g_resize_w = "448"#512#768#1152
#g_resize_h = "336"#384#324#768        
g_resize_w = 448#512#768#1152
g_resize_h = 336#384#324#768        
#os.system("python transfer_resize.py {0} {1} {2} {3} {4}".format(
#        kneron_config.DATASET_TRAIN_PATH, 
#        absoluteMergeTrainPath, 
#        absoluteResizeTrainPath, 
#        g_resize_w, g_resize_h))

CHECK_TFRECORD_FILE=0
#srcDatasetTrainPath = "/home/projects/datasets/1/train"
#srcDatasetTestPath = "/home/projects/datasets/1/test"
#dstDatasetTrainPath = os.path.join(dstDatasetPath, kneron_config.TRAIN_NAME)
#dstDatasetTestPath = os.path.join(dstDatasetPath, kneron_config.TEST_NAME)

IMAGE_PRE_PROCESSING=1

absoluteMergePath = os.path.join(dstDatasetPath, kneron_config.MERGE_NAME)#, kneron_config.TRAIN_NAME) 
absoluteResizePath = os.path.join(dstDatasetPath, kneron_config.RESIZE_NAME)#, kneron_config.TRAIN_NAME)  
absoluteAugmentationPath = os.path.join(dstDatasetPath, kneron_config.AUGMENT_NAME)#, kneron_config.TRAIN_NAME)  
absoluteImgProcTrainPath = os.path.join(dstDatasetPath, kneron_config.IMGPROC_NAME, kneron_config.TRAIN_NAME) 
absoluteImgProcTestPath = os.path.join(dstDatasetPath, kneron_config.IMGPROC_NAME, kneron_config.TEST_NAME)  
absoluteDataPath = os.path.join(dstDatasetPath, kneron_config.DATA_NAME)  
absoluteDataTrainCsvFilePath = os.path.join(absoluteDataPath, kneron_config.TRAIN_NAME + kneron_config.EXT_csv) 
absoluteDataTestCsvFilePath = os.path.join(absoluteDataPath, kneron_config.TEST_NAME + kneron_config.EXT_csv) 
absoluteDataTrainRecordFilePath = os.path.join(absoluteDataPath, kneron_config.TRAIN_NAME + kneron_config.EXT_record) 
absoluteDataTestRecordFilePath = os.path.join(absoluteDataPath, kneron_config.TEST_NAME + kneron_config.EXT_record) 


if IMAGE_PRE_PROCESSING == 1:
    
    
    if os.path.exists(absoluteMergePath):
        shutil.rmtree(absoluteMergePath)
    ek_common.mkdir(absoluteMergePath)
    
        
    if os.path.exists(absoluteResizePath):
        shutil.rmtree(absoluteResizePath)
    ek_common.mkdir(absoluteResizePath)
        
    if os.path.exists(absoluteAugmentationPath):
        shutil.rmtree(absoluteAugmentationPath)
    ek_common.mkdir(absoluteAugmentationPath)
    
    if os.path.exists(absoluteImgProcTrainPath):
        shutil.rmtree(absoluteImgProcTrainPath)
    ek_common.mkdir(absoluteImgProcTrainPath)
        
    if os.path.exists(absoluteImgProcTestPath):
        shutil.rmtree(absoluteImgProcTestPath)
    ek_common.mkdir(absoluteImgProcTestPath)
    
    if os.path.exists(absoluteDataPath):
        shutil.rmtree(absoluteDataPath)
    ek_common.mkdir(absoluteDataPath)


    REWORK_2018_09=0
    if REWORK_2018_09 == 1:
        reworkSrcPath="/home/projects/datasets/full/train/2018_09/james_datasets_61_216_153_171"
        reworkDstPath="/home/projects/datasets/full/train/2018_09/james_datasets_61_216_153_171_rework"
        absoluteReworkCarPath = os.path.join(reworkDstPath, kneron_config.REWORK_CAR_NAME)#, kneron_config.TRAIN_NAME) 
        absoluteReworkTruckPath = os.path.join(reworkDstPath, kneron_config.REWORK_TRUCK_NAME)#, kneron_config.TRAIN_NAME) 
        
        if os.path.exists(absoluteReworkCarPath):
            shutil.rmtree(absoluteReworkCarPath)
        ek_common.mkdir(absoluteReworkCarPath)
        if os.path.exists(absoluteReworkTruckPath):
            shutil.rmtree(absoluteReworkTruckPath)
        ek_common.mkdir(absoluteReworkTruckPath)
        
        import transfer_resize
        transfer_resize.transferJPGTojpgMix(reworkSrcPath, 
                                            absoluteReworkCarPath, 
                                            absoluteReworkTruckPath, 
                                            0, 0)


    import transfer_resize
    transfer_resize.transferJPGTojpg(kneron_config.DATASET_TRAIN_PATH, 
                                     absoluteMergePath, 
                                     absoluteResizePath, 
                                     g_resize_w, g_resize_h)

    #os.system("python img_processing.py {0} {1} {2} {3}".format(
    #        '1',
    #        kneron_config.DATASET_TRAIN_PATH, 
    #        absoluteResizeTrainPath, 
    #        absoluteImgProcTrainPath))
    import img_processing
    img_processing.copyTo(absoluteResizePath, absoluteAugmentationPath)
    img_processing.imageAugmentation(absoluteAugmentationPath)
    
    import imgset_split
    imgset_split.split(absoluteAugmentationPath, 
                       absoluteImgProcTrainPath,
                       absoluteImgProcTestPath)
  
    import xml_to_csv
    xml_to_csv.convert(absoluteImgProcTrainPath, absoluteDataTrainCsvFilePath)
    xml_to_csv.convert(absoluteImgProcTestPath, absoluteDataTestCsvFilePath)
    
    
    #os.chdir('../models-master/research')
    #sys.path.append('../models/research')
    
    
    import generate_tfrecord
    train_tfrecord_generator = generate_tfrecord.TfRecordGenerator(absoluteImgProcTrainPath, absoluteDataTrainCsvFilePath, absoluteDataTrainRecordFilePath)
    train_tfrecord_generator.generate()#absoluteImgProcTestPath, absoluteDataTestCsvFilePath, absoluteDataTestRecorFilePath)
    #generate_tfrecord.generate(absoluteDataTrainCsvFilePath, absoluteDataTrainRecordFilePath)
    test_tfrecord_generator = generate_tfrecord.TfRecordGenerator(absoluteImgProcTestPath, absoluteDataTestCsvFilePath, absoluteDataTestRecordFilePath)
    test_tfrecord_generator.generate()#absoluteImgProcTestPath, absoluteDataTestCsvFilePath, absoluteDataTestRecorFilePath)

import read_tfrecord
if CHECK_TFRECORD_FILE == 1:
    read_tfrecord.read(absoluteImgProcTrainPath, absoluteImgProcTestPath)
#    for example in tf.python_io.tf_record_iterator(absoluteDataTrainRecordFilePath):
#        result = tf.train.Example.FromString(example)
    
    


#
#python3 train.py –logtostderr --train_dir=training/​ --pipeline_config_path=training/ssd_mobilenet_v1_coco.config​
#
#
#MODEL_NAME = "ssd_mobilenet_v1_0.75_depth_300x300_coco14_sync_2018_07_03"
###MODEL_NAME = "ssd_mobilenet_v1_coco_2018_01_28"
###MODEL_NAME = "ssd_mobilenet_v1_ppn_shared_box_predictor_300x300_coco14_sync_2018_07_03"
##
### Path to frozen detection graph. This is the actual model that is used for the object detection
#PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
##
### List of the strings that is used to add correct label for each box.
#PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
##
#NUM_CLASSES = 1
#
## Label maps map indices to category name, so that when our convolution network predicts 5, we
## know that this corresponds to airplane. Here we use internal utility functions.
## but anything that returns a dictionary mapping integers to appropriate string labels would be find
#label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
#categories = label_map_util.convert_label_map_to_categories(
#        label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
#categories_index = label_map_util.create_category_index(categories)
#



# TEST_IMAGE_PATHS = { os.path.join(PATH_TO_TEST_IMAGES_DIR, 'beach{}.jpg'.format(i)) for i in range(1,3) ]


#train_tfrecord_generator.read()


#
#from shlex import split
#os.chdir(AUGMENTATION_DATASET_DIR)
#p1 = subprocess.Popen(split("ls -1q"),stdout=subprocess.PIPE,stderr=subprocess.PIPE)
##p2 = subprocess.Popen(split("wc -l"),stdout=subprocess.PIPE,stderr=subprocess.PIPE)
#p2 = subprocess.Popen(split("wc -l"),stdin=p1.stdout,stdout=subprocess.PIPE)
##output, errors = p1.communicate()
#output, errors = p2.communicate()
#
#print(output)
#os.chdir("..")
