"""
Usage:
  # From tensorflow/models/
  # Create train data:
  python3 generate_tfrecord.py --csv_input=data/train_labels.csv  --output_path=data/train.record

  # Create test data:
  python3 generate_tfrecord.py --csv_input=data/test_labels.csv  --output_path=data/test.record
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

#import os
#import io

#import tensorflow as tf

#from PIL import Image



#flags = tf.app.flags
#flags.DEFINE_string('csv_input', '', 'Path to the CSV input')
#flags.DEFINE_string('output_path', '', 'Path to output TFRecord')
#FLAGS = flags.FLAGS

import os
import sys
import io
import numpy as np
#from scipy import misc
import pandas as pd
import tensorflow as tf
from PIL import Image
from collections import namedtuple, OrderedDict

sys.path.append('../models-master/research')
os.system("export PYTHONPATH=$PYTHONPATH:pwd:pwd/slim")
from object_detection.utils import dataset_util

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_string("input", "", "Input folder containing images")
tf.app.flags.DEFINE_string("output", "", "Output tfrecord.")


#
## TO-DO replace this with label map
#def class_text_to_int(row_label):
#    if row_label == 'car':
#        return 1
#    elif row_label == 'motorcycle':
#        return 5
#    elif row_label == 'bus':
#        return 7
#    elif row_label == 'truck':
#        return 9
#    elif row_label == 'traffic_light':
#        return 11
#    else:
#        None
#
#
#def split(df, group):
#    data = namedtuple('data', ['filename', 'object'])
#    gb = df.groupby(group)
#    return [data(filename, gb.get_group(x)) for filename, x in zip(gb.groups.keys(), gb.groups)]
#
#

def class_text_to_int(row_label):
    if row_label == 'car':
        return 1
    elif row_label == 'motorcycle':
        return 5
    elif row_label == 'bus':
        return 7
    elif row_label == 'truck':
        return 9
    elif row_label == 'traffic_light':
        return 11
    else:
        None

#Converts image and annotations to a tf.Example proto.
#create_tf_example(subset, self.src_img_dir)
        #image, annotations_list, image_dir, category_index, include_masks
def create_tf_example(group, path):
    with tf.gfile.GFile(os.path.join(path, '{}'.format(group.filename)), 'rb') as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = Image.open(encoded_jpg_io)
    width, height = image.size

    filename = group.filename.encode('utf8')
    image_format = b'jpg'
    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []

    for index, row in group.object.iterrows():
        xmins.append(row['xmin'] / width)
        xmaxs.append(row['xmax'] / width)
        ymins.append(row['ymin'] / height)
        ymaxs.append(row['ymax'] / height)
        classes_text.append(row['class'].encode('utf8'))
        classes.append(class_text_to_int(row['class']))

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_jpg),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


def split(text, columnName):
    data = namedtuple('data', ['filename', 'object'])
    objGroup = text.groupby(columnName)
#    print('type(objGroup)=',type(objGroup)) #<class 'pandas.core.groupby.groupby.DataFrameGroupBy'>
#    print('objGroup.groups=',objGroup.groups)    
#    print('objGroup.size()=',objGroup.size())
#    for k, gp in objGroup:
#        print('kkk=',objGroup.get_group(k))
#        
    #print('objGroup.groups=',objGroup.groups)
#    for filename, x, in zip(objGroup.groups.keys(), objGroup.groups):#zip(objGroup.groups.keys(), objGroup.groups):
#        print('filename=', filename)
#        print('type(filename)=', type(filename))
#        print('type(x)=', type(x))
#        #print(x)
#        print('x1=',objGroup.get_group(filename))
#        print('x2=',objGroup.get_group(x))
        #print('key=', str(k))
        #print('gp=',gp)
    #data(filename, gb.get_group(x)) for filename
#    print('gb.groups=',objGroup.groups)
#    print('gb.groups.keys()=',gb.groups.keys())
#    zip(gb.groups.keys(), gb.groups)
    #for t in gb:
    #    print('t=', t)
    return [data(filename, objGroup.get_group(subset)) for filename, subset in zip(objGroup.groups.keys(), objGroup.groups)]

#def generate(srcImgDir, csvFilePath, tfrecordFilePath):
##    print('FLAGS.input=',FLAGS.input)
##    print('FLAGS.output=',FLAGS.output)
##    print('csvFilePath=',csvFilePath)
##    print('tfrecordFilePath=',tfrecordFilePath)
#    if os.path.exists(csvFilePath):
#        csvText = pd.read_csv(csvFilePath)
#        #csvText = pd.read_csv(FLAGS.input)
#        groupedData = split(csvText, 'filename')
#        for subset in groupedData:
#            tf_example = create_tf_example(subset, srcImgDir)
#            writer.write(tf_example.SerializeToString())
#        #for group in grouped:
#        #    print('group=',group)

from google.protobuf.json_format import MessageToJson

class TfRecordGenerator():
    def __init__(self, srcImgDir, csvFilePath, tfrecordFilePath):
        self.src_img_dir = srcImgDir
        self.cvs_file_path = csvFilePath
        self.tfrecord_file_path = tfrecordFilePath        
        self.writer = tf.python_io.TFRecordWriter(tfrecordFilePath)
        #self.writer = tf.python_io.TFRecordWriter(FLAGS.output)

    def generate(self):
    #    print('FLAGS.input=',FLAGS.input)
    #    print('FLAGS.output=',FLAGS.output)
    #    print('csvFilePath=',csvFilePath)
    #    print('tfrecordFilePath=',tfrecordFilePath)
        print('self.src_img_dir=',self.src_img_dir)
        print('self.cvs_file_path=',self.cvs_file_path)
        print('self.tfrecord_file_path=',self.tfrecord_file_path)
        if os.path.exists(self.cvs_file_path):
            csvText = pd.read_csv(self.cvs_file_path)
            groupedData = split(csvText, 'filename')
            for subset in groupedData:
                tf_example = create_tf_example(subset, self.src_img_dir)
                self.writer.write(tf_example.SerializeToString())
                
#            reader = tf.TFRecordReader(name=None, options=None)
#            filename_queues = tf.train.string_input_producer([self.tfrecord_file_path])
#            _,serialized_example = reader.read(filename)
#            for example in tf.python_io.tf_record_iterator(self.tfrecord_file_path):
#                jsonMessage = MessageToJson(tf.train.Example.FromString(example))
#                #result = tf.train.Example.FromString(example)
#                print(jsonMessage)
            #for group in grouped:
            #    print('group=',group)

#    def read(self):
#        filename_queues = tf.train.string_input_producer([self.tfrecord_file_path])
#        # TFRecords文件的压缩选项
#        options_zlib = tf.python_io.TFRecordOptions(tf.python_io.TFRecordCompressionType.ZLIB)
#        options_gzip = tf.python_io.TFRecordOptions(tf.python_io.TFRecordCompressionType.GZIP)
#        # 定义不同压缩选项的TFRecordReader
#        reader_none = tf.TFRecordReader(options=None)
#        reader_zlib = tf.TFRecordReader(options=options_zlib)
#        reader_gzip = tf.TFRecordReader(options=options_gzip)
#        # 读取不同的tfrecord文件
#        _,serialized_example_none = reader_none.read(filename_queues)
#        _,serialized_example_zlib = reader_zlib.read(filename_queues)
#        _,serialized_example_gzip = reader_gzip.read(filename_queues)
#        # 根据key名字得到保存的features字典
#        features_none = tf.parse_single_example(serialized_example_none,
#        features={
#        "filename":tf.FixedLenFeature([], tf.string),
#        "width":tf.FixedLenFeature([], tf.int64),
#        "height":tf.FixedLenFeature([], tf.int64),
#        "class":tf.FixedLenFeature([], tf.string)
#        })
#        features_zlib = tf.parse_single_example(serialized_example_zlib,
#        features={
#        "filename":tf.FixedLenFeature([], tf.string),
#        "width":tf.FixedLenFeature([], tf.int64),
#        "height":tf.FixedLenFeature([], tf.int64),
#        "class":tf.FixedLenFeature([], tf.string)
#        })
#        features_gzip = tf.parse_single_example(serialized_example_gzip,
#        features={
#        "filename":tf.FixedLenFeature([], tf.string),
#        "width":tf.FixedLenFeature([], tf.int64),
#        "height":tf.FixedLenFeature([], tf.int64),
#        "class":tf.FixedLenFeature([], tf.string)
#        })
##        # 保存时是以image原始格式数据，读出来后，还需要解码，只显示features_gzip的图片即可
##        image = tf.image.decode_jpeg(features_gzip['image_raw'], channels=3)
##        # 启用队列协调管理器，并使用tf.train.start_queue_runners启动队列文件线程
##        coord = tf.train.Coordinator()
##        threads = tf.train.start_queue_runners(sess=sess, coord=coord)
##        # 获取不同tfrecord文件保存的float_val的值，正确的值应该是9.99/8.88/6.66
#        sess = tf.Session()
#        float_val_none,float_val_zlib,float_val_gzip = sess.run([features_none['filename'], features_zlib['filename'], features_gzip['filename']])
##        # 关闭线程以及会话
##        coord.request_stop()
##        coord.join(threads)
##        sess.close()
##        # 打印获取到不同tfrecord文件里的float_val，以验证正确性
#        print(float_val_none)
#        print(float_val_zlib)
#        print(float_val_gzip)
#        # 显示读取到的图片
##        plt.imshow(image)
##        plt.title("beautiful view")
##        plt.show()
##        
##        print("finish to read data from tfrecord file!")

def main(argv):
#    a = [1,2,3]
#    b = [4,5,6]
#    c = [4,5,6,7,8]
#    zipped = zip(a,b)
#    print('zipped=',zipped)
    del argv
    generate()
  
#def main(_):
#    writer = tf.python_io.TFRecordWriter(FLAGS.output_path)
#    path = os.path.join(os.getcwd(), 'images/train')
#    examples = pd.read_csv(FLAGS.csv_input)
#    grouped = split(examples, 'filename')
#    for group in grouped:
#        tf_example = create_tf_example(group, path)
#        writer.write(tf_example.SerializeToString())
#
#    writer.close()
#    output_path = os.path.join(os.getcwd(), FLAGS.output_path)
#    print('Successfully created the TFRecords: {}'.format(output_path))


if __name__ == '__main__':
    tf.app.run()
