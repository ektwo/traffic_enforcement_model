import os
import sys
import glob
import cv2
import numpy as np
import xml.etree.ElementTree as ET
import kneron_config
import ek_common
import modify_xml

from skimage import exposure
from skimage import color
from skimage import img_as_ubyte
from scipy import ndimage   


def skimage2opencv(src):
    src *= 255
    src.astype(int)
    cv2.cvtColor(src,cv2.COLOR_RGB2BGR)
    return src

def opencv2skimage(src):
    cv2.cvtColor(src,cv2.COLOR_BGR2RGB)
    src.astype(np.float32)
    src = src/255
    return src

def _intensity(img, *argv):
    #for arg in argv:
    #    print("_intensity *argv :", arg,len(arg),arg[0])
    #print('argv[0]=',argv[0])
    #print('argv[1]=',argv[1])
    #a1=''.join(argv[0][0])
    #a2=''.join(argv[0][1])
    percentLow = int(argv[0][0])
    percentHigh = int(argv[0][1])    
    #The value exceeed valueMax or low than valueMin will set to 1
    #v_min, v_max = np.percentile(img, (1, 80))
    valueMin, valueMax = np.percentile(img, (percentLow, percentHigh))
    intensity = exposure.rescale_intensity(img, in_range=(valueMin, valueMax))
    return (intensity)

def _equalize_hist(img, *argv):
    #print('img.shape[2]=',img.shape[2])
    
    img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

    # equalize the histogram of the Y channel
    img_yuv[:,:,0] = cv2.equalizeHist(img_yuv[:,:,0])

    # convert the YUV image back to RGB format
    img_output = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)

    return (img_output)



def _equalize_adapthis(img, *argv):
    clipLimitVal = float(argv[0][0])   
    
    hsv = color.rgb2hsv(img)
    hsv[:, :, 2] = exposure.equalize_adapthist(hsv[:, :, 2], clip_limit=clipLimitVal)
    img_adapteq = color.hsv2rgb(hsv)
    
    cv_image = img_as_ubyte(img_adapteq)

    return (cv_image)

def _gamma(img, *argv):
    gammaVal = float(argv[0][0])    
    gainVal = float(argv[0][1])
    #print('gammaVal=',gammaVal)
    #print('gainVal=',gainVal)
    adjusted_gamma_image = exposure.adjust_gamma(img, gamma=gammaVal, gain=gainVal)
    return(adjusted_gamma_image)

#def _gamma2(img, *argv):
#    adjusted_gamma_image2 = exposure.adjust_gamma(img, gamma=0.4, gain=0.6)
#    return(adjusted_gamma_image2)

def _sigmoid(img, *argv):
    sigmoid_correction_image = exposure.adjust_sigmoid(img)
    return(sigmoid_correction_image)

def _blured(img, *argv):
    blured_image = ndimage.uniform_filter(img, size=(5, 5, 2))
    return(blured_image)

def _augmentate_img(function, imgFilePath, xmlFilePath, mirror, *argv):
    #print("func name is", function.__name__, xmlFilePath)
    img = cv2.imread(imgFilePath)
    if mirror is True:
        img = cv2.flip(img, mirror)

    absImgPath = imgFilePath.split(kneron_config.EXT_jpg)[0]
    absXmlPath = xmlFilePath.split(kneron_config.EXT_xml)[0]
 
    if mirror is True:
        dstAugPath_Img = absImgPath + '_' + 'mirror_' + function.__name__
    else:
        dstAugPath_Img = absImgPath + '_' + function.__name__
    for arg in argv:
        if arg.isdigit():
            dstAugPath_Img = dstAugPath_Img + '_' + arg
        else:
            fVal = float(arg)
            #print('fVal=',fVal)
            nVal = int(fVal)
            fVal = fVal- nVal
            strCarrys = list(str(nVal) + 'd')
            ek_common.roundup_bigthan1(fVal, strCarrys)
            #print('strCarrys=',strCarrys)         
            dstAugPath_Img = dstAugPath_Img + '_' + ''.join(strCarrys)
    
    dstAugPath_Img = dstAugPath_Img + kneron_config.EXT_jpg
    #print('_augmentate_img imgFile=',dstAugPath_Img)
                
    if mirror is True:
        dstAugPath_Xml = absXmlPath + '_' + 'mirror_' + function.__name__
    else:
        dstAugPath_Xml = absXmlPath + '_' + function.__name__    
    for arg in argv:
        if arg.isdigit():
            dstAugPath_Xml = dstAugPath_Xml + '_' + arg
        else:
            fVal = float(arg)
            #print('fVal=',fVal)
            nVal = int(fVal)
            fVal = fVal- nVal
            strCarrys = list(str(nVal) + 'd')
            ek_common.roundup_bigthan1(fVal, strCarrys)
            #print('strCarrys=',strCarrys)                
            dstAugPath_Xml = dstAugPath_Xml + '_' + ''.join(strCarrys)
        
    dstAugPath_Xml = dstAugPath_Xml + kneron_config.EXT_xml
   
    #print('_augmentate_img xmlFile=',dstAugPath_Xml)                   

    img_aug = function(img, argv)
    cv2.imwrite(dstAugPath_Img, img_aug)

    
    modify_xml.modifyTo(xmlFilePath, dstAugPath_Xml, mirror)


def copyTo(sourcePath, dstAugmentationPath):
    for dirpath, dirnames, filenames in os.walk(sourcePath, topdown=True):    
        for filename in filenames:
            name, ext = os.path.splitext(filename)
            if ext == kneron_config.EXT_jpg:
                fileNameXml = filename.split(ext)[0] + kneron_config.EXT_xml # /1/2/2.xml
                absSrcPath_Xml = os.path.join(dirpath, fileNameXml)
                if os.path.isfile(absSrcPath_Xml) == False:
                    continue

                absSrcPath_Img = os.path.join(dirpath, filename)
                #relPath = os.path.relpath(dirpath, sourcePath)
                             
                absoluteAugmentationPath_ImgFile = os.path.join(dstAugmentationPath, filename)                 
                absoluteAugmentationPath_XmlFile = os.path.join(dstAugmentationPath, fileNameXml)           
                #print("absoluteAugmentationPath_ImgFile={0}".format(absoluteAugmentationPath_ImgFile))
                #print("absoluteAugmentationPath_XmlFile={0}".format(absoluteAugmentationPath_XmlFile))
                ek_common.copyfile(absSrcPath_Img, absoluteAugmentationPath_ImgFile) 
                ek_common.copyfile(absSrcPath_Xml, absoluteAugmentationPath_XmlFile)
                
                #tree = ET.parse(absoluteAugmentationPath_XmlFile)  
                modify_xml.modify(absoluteAugmentationPath_XmlFile)

def imageAugmentation(dstAugmentationPath):
    #filelist_xml=glob.glob(dstAugmentationPath + "*" + kneron_config.EXT_jpg)
    #absolutePath_jpgFile = os.path.join(sourcePath, "*", kneron_config.EXT_jpg)                 
    filelist_jpg = glob.glob(dstAugmentationPath + "/*" + kneron_config.EXT_jpg)
    
    #filelist_jpg = glob.glob(absolutePath_jpgFile)
    #print("absolutePath_jpgFile is ",absolutePath_jpgFile)
    #print("dstAugmentationPath is ",dstAugmentationPath)
    #print("filelist_jpg is ",filelist_jpg)
    #print("filelist_xml is ",filelist_xml)
    #print (os.path.split(filelist_jpg[0])[-1])
    
    #filelist_xml=chck_jpg_xml_files(filelist_jpg)#,filelist_xml)

    for img_idx in range(0, len(filelist_jpg)):
        xmlfile = filelist_jpg[img_idx].split(kneron_config.EXT_jpg)[0] + kneron_config.EXT_xml
#        relPath = os.path.relpath(filelist_jpg[img_idx], sourcePath)
        #print("relPath={0}".format(relPath))
        #print("filelist_jpg[{0}]={1} ".format(img_idx,filelist_jpg[img_idx]))
        #print("xmlfile[{0}]={1} ".format(img_idx,xmlfile))
        _augmentate_img(_intensity, filelist_jpg[img_idx],xmlfile, False, '1', '80')
        _augmentate_img(_intensity, filelist_jpg[img_idx],xmlfile, False, '20', '99')
        _augmentate_img(_equalize_hist, filelist_jpg[img_idx],xmlfile, False)
        _augmentate_img(_equalize_adapthis, filelist_jpg[img_idx],xmlfile, False, '0.03')
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, False, '0.4', '0.9')
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, False, '0.4', '0.6')
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, False, '1.5', '1')
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, False, '2', '1')        
        _augmentate_img(_sigmoid,filelist_jpg[img_idx],xmlfile, False)
        _augmentate_img(_blured,filelist_jpg[img_idx],xmlfile, False)
        
        _augmentate_img(_intensity, filelist_jpg[img_idx],xmlfile, True, '1', '80')
        _augmentate_img(_intensity, filelist_jpg[img_idx],xmlfile, True, '20', '99')
        _augmentate_img(_equalize_hist, filelist_jpg[img_idx],xmlfile, True)
        _augmentate_img(_equalize_adapthis, filelist_jpg[img_idx],xmlfile, True, '0.03')        
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, True, '0.4', '0.9')
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, True, '0.4', '0.6')
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, True, '1.5', '1')
        _augmentate_img(_gamma,filelist_jpg[img_idx],xmlfile, True, '2', '1')        
        _augmentate_img(_sigmoid,filelist_jpg[img_idx],xmlfile, True)
        _augmentate_img(_blured,filelist_jpg[img_idx],xmlfile, True)        
        #augmentation_img(rotate,filelist_jpg[img_idx],xmlfile,'90')
        
#        augmentation_img(intensity,filelist_jpg[img_idx],filelist_xml[img_idx])
#        augmentation_img(gamma,filelist_jpg[img_idx],filelist_xml[img_idx])
#        augmentation_img(gamma2,filelist_jpg[img_idx],filelist_xml[img_idx])
#        augmentation_img(sigmoid,filelist_jpg[img_idx],filelist_xml[img_idx])
#        augmentation_img(blured,filelist_jpg[img_idx],filelist_xml[img_idx])
#        augmentation_img(rotate,filelist_jpg[img_idx],filelist_xml[img_idx],'90')
        
#        #cv2.imshow("test", img)
#        #cv2.waitKey(1000)
        
def main():        
    copyTo(sys.argv[3], sys.argv[4])
#    print('bool(sys.argv[1])=',bool(int(sys.argv[1])))
#    print('sys.argv[3]=',sys.argv[3])
#    print('sys.argv[4]=',sys.argv[4])
    if bool(int(sys.argv[1])) == True:
        imageAugmentation(sys.argv[4])
    #ek_common.copyfile(sys.argv[1], sys.argv[4]) 
    
if __name__ == "__main__":
    main()