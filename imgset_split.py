#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 01:37:35 2018

@author: ektwo
"""
import os
import sys
import numpy as np
import kneron_config
import ek_common
import modify_xml
from sklearn.model_selection import train_test_split

#X, y = np.arange(10).reshape((5, 2)), range(5)
#print(X)
#print(list(y))
#range(10)

#train_part, test_part = train_test_split(X, test_size = 0.2, random_state = 0)
#print('train_part',train_part)
#print('test_part',test_part)
#def splitTrainTest(sourcePathList, dstTrainPathList, dstTestPathList):
#    dstTrainPathList, dstTestPathList = train_test_split(sourcePathList, test_size = 0.2, random_state = 0)
#    print('dstTrainPathList',dstTrainPathList)
#    print('dstTestPathList',dstTestPathList)
#
class ImagePair():
    def __init__(self, imgPath, srcXmlPath, dstXmlPath):
        self.img_path = imgPath
        self.src_xml_path = srcXmlPath
        self.dst_xml_path = dstXmlPath
        
        
def split(sourcePath, dstTrainPath, dstTestPath):
    imagePathList = []
    for dirpath, dirnames, filenames in os.walk(sourcePath, topdown=True):  
        for filename in filenames:
            name, ext = os.path.splitext(filename)
            if ext == kneron_config.EXT_jpg:
                absSrcPath_Img = os.path.join(dirpath, filename)
                if os.path.isfile(absSrcPath_Img) is False:
                    continue
                imagePathList.append(absSrcPath_Img)

    print("XX len(imagePathList)=",len(imagePathList))         

    trainImgPathList, testImgPathList = train_test_split(imagePathList, test_size = 0.2, random_state = 0)


    imgPairTrainList = []
    imgPairTestList = []
    for filepath in trainImgPathList:
        name, ext = os.path.splitext(filepath)
        absSrcPath_Xml = name + kneron_config.EXT_xml
        xmlFileName = absSrcPath_Xml.split('/')[-1]
        absDstPath_Xml = os.path.join(dstTrainPath, xmlFileName)
        imgPairTrainList.append(ImagePair(filepath, absSrcPath_Xml, absDstPath_Xml))

    for filepath in testImgPathList:
        name, ext = os.path.splitext(filepath)
        absSrcPath_Xml = name + kneron_config.EXT_xml
        xmlFileName = absSrcPath_Xml.split('/')[-1]
        absDstPath_Xml = os.path.join(dstTestPath, xmlFileName)
        imgPairTestList.append(ImagePair(filepath, absSrcPath_Xml, absDstPath_Xml))
        
    #print("XX len(imgPairTrainList)=",len(imgPairTrainList))
    for img_pair in imgPairTrainList:
        #print("imgPairTrainList.img_path=",img_pair.img_path)
        #print("imgPairTrainList.src_xml_path=",img_pair.src_xml_path)
        #print("imgPairTrainList.dst_xml_path=",img_pair.dst_xml_path)
        ek_common.copyfile(img_pair.img_path, dstTrainPath) 
        ek_common.copyfile(img_pair.src_xml_path, dstTrainPath)
        modify_xml.modify(img_pair.dst_xml_path)     
        
    #print("XX len(imgPairTestList)=",len(imgPairTestList))
    for img_pair in imgPairTestList:
        #print("imgPairTestList.img_path=",img_pair.img_path)
        #print("imgPairTestList.src_xml_path=",img_pair.src_xml_path)
        #print("imgPairTestList.dst_xml_path=",img_pair.dst_xml_path)
        ek_common.copyfile(img_pair.img_path, dstTestPath) 
        ek_common.copyfile(img_pair.src_xml_path, dstTestPath)
        modify_xml.modify(img_pair.dst_xml_path)                 
#    print("XX len(test_imgset)=",len(test_imgset))
#    for imgpair in test_imgset_path:
#        print("test_imgset img_name=",test_imgset.img_name)
#        print("test_imgset xml_name=",test_imgset.xml_name)                
#
#        fileNameXml = test_imgset.xml_name.split('/')[-1]
#        print("train_imgset fileNameXml=",fileNameXml)
#        #absoluteAugmentationPath_ImgFile = os.path.join(dstAugmentationPath, filename)                 
#        absoluteDstPath_XmlFile = os.path.join(dstTestPath, fileNameXml)                   
#        print("train_imgset absoluteDstPath_XmlFile=",absoluteDstPath_XmlFile)
#        ek_common.copyfile(train_imgset.img_name, dstTestPath) 
#        ek_common.copyfile(train_imgset.xml_name, dstTestPath)
#        modify_xml.modify(absoluteDstPath_XmlFile)     

def main():        
    #print("transferExtFromJPGTojpg= argv:%s argv:%s".format(sys.argv[1],sys.argv[2],sys.argv[3])) 
    split(sys.argv[1], sys.argv[2], sys.argv[3])    
    
if __name__ == "__main__":
    main()        
