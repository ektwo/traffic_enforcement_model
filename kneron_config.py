# -*- coding: utf-8 -*-

#import sys 
#import globalvar as gl 
#
#gl._init() 
#gl.set_value('MERGE_NAME', 'merge') 
#gl.set_value('RESIZE_NAME', 'resize') 

#srcDatasetTrainPath = "/home/projects/datasets/1/train"
#srcDatasetTestPath = "/home/projects/datasets/1/test"
DATASET_TRAIN_PATH = "/home/projects/datasets/1/train"
DATASET_TEST_PATH = "/home/projects/datasets/1/test"

IMG_PARENT_NAME="images"
MERGE_NAME="1_merge"
REWORK_CAR_NAME="rework_car"
REWORK_TRUCK_NAME="rework_truck"
RESIZE_NAME="2_resize"
AUGMENT_NAME="3_augmentation"
IMGPROC_NAME="4_images"
DATA_NAME="data"

TRAIN_NAME="train"
TEST_NAME="test"
#IMGPROC_TRAIN_NAME="images/train"
#IMGPROC_TEST_NAME="images/test"

EXT_JPG='.JPG'
EXT_jpg='.jpg'
EXT_xml='.xml'
EXT_csv='.csv'
EXT_record='.record'