# -*- coding: utf-8 -*-

import os
import xml.etree.ElementTree as ET
import kneron_config

def modify(xmlFilePath):
    #print("===== modify ======")
    dstXmlPath = xmlFilePath.split(kneron_config.EXT_xml)[0]
    #print("modify dstXmlPath is ",dstXmlPath)

    imgPath = dstXmlPath + kneron_config.EXT_jpg
    imgFileName = os.path.split(imgPath)[-1]
    #print("modifyTo imgPath is ", imgPath)
    #print("modifyTo imgFileName is ", imgFileName)
    tree = ET.parse(xmlFilePath)
    root = tree.getroot()


    for elem in root.iter('filename'):
        #print('filename  tag is ', elem.text)
        elem.text=str(imgFileName)
        #print('change filename  tag is ', elem.text)

    for elem in root.iter('path'):
        #print('path tag is ', elem.text,type(elem.text))
        #org_path=elem.text.split(elem.text.split('\\')[-1])[0]
        #print("XXX org_path is",org_path)
        #new_path=org_path+jpg_file_name
 
        #print("XXX new_path is",imgPath)
        elem.text=str(imgPath)
        #print('XXX change path  tag is ', elem.text)
        
    tree.write(xmlFilePath)  
    
def modifyTo(srcXmlFilePath, dstXmlFilePath, mirror):
    #print("===== modifyTo ======")
    dstXmlPath = dstXmlFilePath.split(kneron_config.EXT_xml)[0]
    #print("modifyTo dstXmlPath is ", dstXmlPath)

    imgPath = dstXmlPath + kneron_config.EXT_jpg
    imgFileName = os.path.split(imgPath)[-1]
    #print("modifyTo imgPath is ",imgPath)
    #print("modifyTo imgFileName is ",imgFileName)
    tree = ET.parse(srcXmlFilePath)
    root = tree.getroot()

    if mirror is True:
        width = 0
    
        for elem in root.iter('width'):
            width = int(elem.text)
    
        #print('mirror width=', width)
            
        for elem in root.iter('xmin'):
            xmin = int(elem.text)
            #print('mirror old xmin =', xmin)
            elem.text = str(width - xmin)
            #print('mirror new xmin =', width - xmin)

        for elem in root.iter('xmax'):
            xmax = int(elem.text)
            #print('mirror old xmax =', xmax)            
            elem.text = str(width - xmax)
            #print('mirror new xmax =', width - xmax)

    for elem in root.iter('filename'):
        #print('filename  tag is ', elem.text)
        elem.text=str(imgFileName)
        #print('change filename  tag is ', elem.text)

    for elem in root.iter('path'):
        #print('path tag is ', elem.text,type(elem.text))
        #org_path=elem.text.split(elem.text.split('\\')[-1])[0]
        #print("XXX org_path is",org_path)
        #new_path=org_path+jpg_file_name
        #new_path = imgPath
        
        #print("XXX new_path is",imgPath)
        elem.text=str(imgPath)
        #print('XXX change path  tag is ', elem.text)
        
    tree.write(dstXmlFilePath)   
    
    
def modifyAll(srcXMLFilePath, 
              dstXMLFilePath, 
              jpgFilePath,
              orgWidth, 
              orgHeight, 
              wantWidth, 
              wantHeight):#ratio_w, ratio_h):
    #print('orgWidth={0} orgHeight={1}'.format(orgWidth, orgHeight))
    if orgWidth is 0 or orgHeight is 0:
        return
        
    ratioW = wantWidth/float(orgWidth)
    ratioH = wantHeight/float(orgHeight)
    tree = ET.parse(srcXMLFilePath)  
    #print(filelist[0].size)
    root = tree.getroot()
    for elem in root.iter('width'):
        #print elem.text
        elem.text=str(wantWidth)
        #print elem.text
    for elem in root.iter('height'):
        #print elem.text
        elem.text=str(wantHeight)
        #print elem.text

    for elem in root.iter('xmin'):
        #print('xmin', elem.text)
        elem.text=str(int(ratioW * float(elem.text)))
        #print('Rxmin', elem.text)
    for elem in root.iter('xmax'):
        #print('xmax', elem.text)
        elem.text=str(int(ratioW * float(elem.text)))
        #print('Rxmax', elem.text)

    for elem in root.iter('ymin'):
        #print('ymin', elem.text)
        elem.text=str(int(ratioH * float(elem.text)))
        #print('Rymin', elem.text)
    for elem in root.iter('ymax'):
        #print('ymax', elem.text)
        elem.text=str(int(ratioH * float(elem.text)))
        #print('Rymax', elem.text)

    for elem in root.iter('filename'):
        #print('filename org=', elem.text)
        #print('filename new=', jpgFilePath)
        elem.text=jpgFilePath
        #print('Rymax', elem.text)
    tree.write(dstXMLFilePath)      