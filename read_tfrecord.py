#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  9 05:16:33 2018

@author: ektwo
"""

import tensorflow as tf
import numpy as np
import os
import sys
def read(filename, filename2): # 读入dog_train.tfrecords
    filename_queue = tf.train.string_input_producer([filename])#生成一个queue队列

    reader = tf.TFRecordReader()
    _, serialized_example = reader.read(filename_queue)#返回文件名和文件
    features = tf.parse_single_example(serialized_example,
                                       features={
                                           'image/object/class/label': tf.FixedLenFeature([], tf.int64)
                                           #'image/filename' : tf.FixedLenFeature([], tf.string)
                                       })#将image数据和label取出来
    #label = features['image/object/class/label']
    #name = features['image/filename']
    with tf.variable_scope('decoder'):
#        label = tf.decode_raw(features['image/object/class/label'], tf.int64)
        label = tf.cast(features['image/object/class/label'], tf.int64)
#        image = tf.decode_raw(features['img_raw'], tf.float32)
#        ground_truth = tf.decode_raw(features['gt_raw'], tf.uint8)
        
         #name = tf.decode_raw(features['image/filename'], tf.uint8)
#    # decode raw image data as integers
#    if self.image_format == 'jpeg':
#        decoded_image = tf.image.decode_jpeg(
#            image, channels=self.image_channels)
#    else:
#        decoded_image = tf.decode_raw(image, tf.uint8)
    
#    with tf.variable_scope('decoder'):
#        label = tf.decode_raw(features['image/object/class/label'], tf.int32)
#        name = features['image/filename']
    
    #print(name)
    #print(label)
    #return label
    with tf.Session() as sess:
        # 初始化是必要的動作
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        
        # 建立執行緒協調器
        coord = tf.train.Coordinator()
        
        # 啟動文件隊列，開始讀取文件
        threads = tf.train.start_queue_runners(coord=coord)
    
        count = 0 
        try:
            # 讀 10 張影像
            while count<10:
                
                # 這邊讀取
                #image_data, label_data = sess.run([image, label])
                label_data = sess.run([label])
                
                # 這邊輸出
                # 因為已經經過解碼，二進制的資料已經轉換成影像檔，因此可以直接使用
                # 影像檔的方式輸出資料。
                #cv2.imwrite('./tf_%d_%d.jpg' % (label_data, count), image_data)
                print(label_data)
                count += 1
    
            print('Done!')        
            
        except tf.errors.OutOfRangeError:
            print('Done!')
    
        finally:
            # 最後要記得把文件隊列關掉
            coord.request_stop()
        
        coord.join(threads)    


#if __name__=='__main__':
#    read(sys.argv[1], sys.argv[2])
    


# TF檔

import skimage.io as io
import cv2
tfrecords_filename = "test.record"

filename_queue = tf.train.string_input_producer([tfrecords_filename]) 
reader = tf.TFRecordReader()
_, serialized_example = reader.read(filename_queue) 
    
features = tf.parse_single_example(serialized_example,
                                   features={
                                        'image/width':tf.FixedLenFeature([], tf.int64),
                                        'image/height': tf.FixedLenFeature([], tf.int64),
                                        'image/filename':  tf.FixedLenFeature([], tf.string),
                                        'image/source_id': tf.FixedLenFeature([], tf.string),
                                        'image/encoded': tf.FixedLenFeature([], tf.string),
                                        'image/format':  tf.FixedLenFeature([], tf.string),
                                        'image/object/bbox/xmin': tf.FixedLenFeature([], tf.float32),
                                        'image/object/bbox/xmax': tf.FixedLenFeature([], tf.float32),
                                        'image/object/bbox/ymin':tf.FixedLenFeature([], tf.float32),
                                        'image/object/bbox/ymax':tf.FixedLenFeature([], tf.float32),
                                        'image/object/class/text':tf.FixedLenFeature([], tf.string),
                                        'image/object/class/label': tf.FixedLenFeature([], tf.int64),
                                   })  

width= tf.cast(features['image/width'], tf.int32)
height = tf.cast(features['image/height'], tf.int32)
filename = tf.cast(features['image/filename'], tf.string)
format = tf.cast(features['image/format'], tf.string)
xmin = tf.cast(features['image/object/bbox/xmin'], tf.float32)
xmax = tf.cast(features['image/object/bbox/xmax'], tf.float32)
ymin = tf.cast(features['image/object/bbox/ymin'], tf.float32)
ymax = tf.cast(features['image/object/bbox/ymax'], tf.float32)
text = tf.cast(features['image/object/class/text'], tf.string)
label = tf.cast(features['image/object/class/label'], tf.int64)

image =tf.image.decode_jpeg(features['image/encoded']);
image = tf.reshape(image,tf.stack([height,width,3]))




with tf.Session() as sess: 
    init_op = tf.initialize_all_variables()
    sess.run(init_op)
    coord=tf.train.Coordinator()
    threads= tf.train.start_queue_runners(coord=coord)
    for i in range(5):
        width1,height1,filename1,format1,xmin1,xmax1,ymin1,ymax1,text1,label1,image1=sess.run([width,height,filename,format,xmin,xmax,ymin,ymax,text,label,image])
        print(width1,height1,filename1,format1,xmin1,xmax1,ymin1,ymax1,text1,label1)
        x1,y1=int(xmin1*width1),int(ymin1*height1)
        x2,y2=int(xmax1*width1),int(ymax1*height1)
        io.imshow(cv2.rectangle(np.array(image1),(x1,y1),(x2,y2),(0,255,0),3), cmap = 'gray', interpolation = 'bicubic')
        io.show()
        
    coord.request_stop()
    coord.join(threads)