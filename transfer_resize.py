import os
import sys
import cv2
import kneron_config
import ek_common
import ek_opencvlib
import modify_xml


cnt=0

def transferJPGTojpg(sourcePath, dstMergePath, dstResizePath, wantWidth, wantHeight):
    #print("=== transferExtFromJPGTojpg: sourcePath=", sourcePath)
    #print("=== transferExtFromJPGTojpg: dstMergePath=", dstMergePath)
    #print("=== transferExtFromJPGTojpg: dstResizePath=", dstResizePath)

    for dirpath, dirnames, filenames in os.walk(sourcePath, topdown=True):
        #print("=== transferExtFromJPGTojpg: dirpath=", dirpath)
        if(str(dirpath).find('__pycache__') != -1 or 
           str(dirpath).find(kneron_config.MERGE_NAME) != -1 or
           str(dirpath).find(kneron_config.RESIZE_NAME) != -1):
            continue
#        print("dirpath is ",dirpath)
#        print("dirnames is ",dirnames)
#        print("filenames is ",filenames)
        for filename in filenames:
            name, ext = os.path.splitext(filename)
            if ext == kneron_config.EXT_JPG or ext == kneron_config.EXT_jpg:
                fileNameXml = filename.split(ext)[0] + kneron_config.EXT_xml 
                absSrcPath_Xml = os.path.join(dirpath, fileNameXml)
                if os.path.isfile(absSrcPath_Xml) == False:
                    continue
                
                width = 0
                height = 0
                channels = 0
                absSrcPath_Img = os.path.join(dirpath, filename)
                img = cv2.imread(absSrcPath_Img)
                if img is None:
                    continue                

                height, width, channels = img.shape
                relPath = os.path.relpath(dirpath, sourcePath)
                fileNameJpg = filename.split(ext)[0] + kneron_config.EXT_jpg
                absMergePath_Img = os.path.join(dstMergePath, relPath, fileNameJpg)
                absMergePath_Xml = os.path.join(dstMergePath, relPath, fileNameXml)
                ek_common.copyfile(absSrcPath_Img, absMergePath_Img) 
                ek_common.copyfile(absSrcPath_Xml, absMergePath_Xml)

                if wantWidth <= 0 or wantHeight <= 0:
                    continue
                absResizePath = os.path.join(dstResizePath, relPath)                 
                absResizePath_Img = os.path.join(absResizePath, fileNameJpg)                 
                absResizePath_Xml = os.path.join(absResizePath, fileNameXml)           
#                print("absResizePath={0}".format(absResizePath))
#                print("absResizePath_Img={0}".format(absResizePath_Img))
#                print("absResizePath_Xml={0}".format(absResizePath_Xml))
                ek_common.mkdir(absResizePath)  
#               print("absoluteMergeFilePath_xml=",absoluteMergeFilePath_xml)
                ek_opencvlib.write_img(img, absResizePath_Img, wantWidth, wantHeight)
                modify_xml.modifyAll(absMergePath_Xml,
                                     absResizePath_Xml,
                                     absResizePath_Img,
                                     width,
                                     height,
                                     wantWidth,
                                     wantHeight)            
import re
def transferJPGTojpgMix(sourcePath, dstReworkCarPath, dstReworkTruckPath, wantWidth, wantHeight):
    #print("=== transferExtFromJPGTojpg: sourcePath=", sourcePath)
    #print("=== transferExtFromJPGTojpg: dstMergePath=", dstMergePath)
    #print("=== transferExtFromJPGTojpg: dstResizePath=", dstResizePath)

    for dirpath, dirnames, filenames in os.walk(sourcePath, topdown=True):
        #print("=== transferExtFromJPGTojpg: dirpath=", dirpath)
        if(str(dirpath).find('__pycache__') != -1 or 
           str(dirpath).find(kneron_config.MERGE_NAME) != -1 or
           str(dirpath).find(kneron_config.RESIZE_NAME) != -1):
            continue
#        print("dirpath is ",dirpath)
#        print("dirnames is ",dirnames)
#        print("filenames is ",filenames)
        for filename in filenames:
            name, ext = os.path.splitext(filename)
            if ext == kneron_config.EXT_JPG or ext == kneron_config.EXT_jpg:
#                fileName_CarXml = filename.split(ext)[0] + '_c' + kneron_config.EXT_xml 
#                fileName_TrackXml = filename.split(ext)[0] + '_t' + kneron_config.EXT_xml 
#                absSrcPath_CarXml = os.path.join(dirpath, fileName_CarXml)
#                absSrcPath_TrackXml = os.path.join(dirpath, fileName_TrackXml)
#                if os.path.isfile(absSrcPath_CarXml) == False and os.path.isfile(absSrcPath_TrackXml) == False :
#                    continue
#                
                width = 0
                height = 0
                channels = 0
                absSrcPath_Img = os.path.join(dirpath, filename)
                img = cv2.imread(absSrcPath_Img)
                if img is None:
                    continue                

                height, width, channels = img.shape
                relPath = os.path.relpath(dirpath, sourcePath)

                print('filename=', filename)
                print('absSrcPath_Img=', absSrcPath_Img)


                x1=re.search(r'([A-Za-z0-9_]+)[\s]\(([A-Za-z0-9_]+)\)',filename)
                #x1=re.search(r'([A-Za-z0-9_]+)[\s]\(([A-Za-z0-9_]+)\)(.*)\.',filename)
                #x1.group(0)
                x1.group(1)
                x1.group(2)
                #x1.group(3)
                print('g0=',x1.group(0))
                print('g1=',x1.group(1))
                print('g2=',x1.group(2))
                #print('g3=',x1.group(3))
                
                fileName_CarXml = x1.group(0) + '_c' + kneron_config.EXT_xml 
                fileName_TrackXml = x1.group(0) + '_t' + kneron_config.EXT_xml 
                absSrcPath_CarXml = os.path.join(dirpath, fileName_CarXml)
                absSrcPath_TrackXml = os.path.join(dirpath, fileName_TrackXml)
                if os.path.isfile(absSrcPath_CarXml) == True:
                    absDstPath_Img = os.path.join(dstReworkCarPath, x1.group(1) + '_' + x1.group(2) + kneron_config.EXT_jpg)                    
                    absDstPath_Xml = os.path.join(dstReworkCarPath, x1.group(1) + '_' + x1.group(2) + kneron_config.EXT_xml)                    
                    print('absSrcPath_CarXml=',absSrcPath_CarXml)
                    print('absDstPath_Img=',absDstPath_Img)
                    print('absDstPath_Xml=',absDstPath_Xml)
                    ek_common.copyfile(absSrcPath_Img, absDstPath_Img) 
                    ek_common.copyfile(absSrcPath_CarXml, absDstPath_Xml)
                if os.path.isfile(absSrcPath_TrackXml) == True:
                    absDstPath_Img = os.path.join(dstReworkTruckPath, x1.group(1) + '_' + x1.group(2) + kneron_config.EXT_jpg)                    
                    absDstPath_Xml = os.path.join(dstReworkTruckPath, x1.group(1) + '_' + x1.group(2) + kneron_config.EXT_xml)                    
                    print('absSrcPath_TrackXml=',absSrcPath_TrackXml)
                    print('absDstPath_Img=',absDstPath_Img)
                    print('absDstPath_Xml=',absDstPath_Xml)
                    ek_common.copyfile(absSrcPath_Img, absDstPath_Img)
                    ek_common.copyfile(absSrcPath_TrackXml, absDstPath_Xml)               

                
                #print('0=',re.escape(filename))
                #print('1=',re.match('capture_20180912_1\ \(7\)\.jpg', filename).span())
                #print('2=',re.match('capture_20180912_1\ \(7\)\.jpg', filename))
#                matchObj = re.match( r'(.*) are (.*?) .*', filename, re.M|re.I)
#                if matchObj:
#                   print("matchObj.group() : ", matchObj.group())
#                   print("matchObj.group(1) : ", matchObj.group(1))
#                   print("matchObj.group(2) : ", matchObj.group(2))
#                else:
#                   print("No match!!")
                #m=re.match(r"\(.+?\)", filename).span()
#                print(m)
#                fileNameJpg = filename.split(' ')[0] + kneron_config.EXT_jpg
#                print('fileNameJpg=', fileNameJpg)
                #absoluteReworkCarPath, absoluteReworkTruckPath
                #if os.path.isfile(absSrcPath_CarXml) == True:
                    
                    #print('absSrcPath_CarXml=', absSrcPath_CarXml)
                    #print('fileName_CarXml=', fileName_CarXml)
                    #dstReworkCarPath, dstReworkTruckPath
                    #ek_common.copyfile(absSrcPath_Img, os.path.join(dstReworkCarPath, fileNameJpg))
#                    #absReworkPath_CarImg = os.path.join(absoluteReworkCarPath, relPath, fileNameJpg)
#                    absReworkPath_CarXml = os.path.join(absoluteReworkCarPath, relPath, fileName_CarXml)
#                    ek_common.copyfile(absSrcPath_Img, absReworkPath_CarXml) 
#                    ek_common.copyfile(absSrcPath_Xml, absMergePath_Xml)                    
                #if os.path.isfile(absSrcPath_TrackXml) == True:   
                    #print('absSrcPath_TrackXml=', absSrcPath_TrackXml)
                    #print('fileName_TrackXml=', fileName_TrackXml)
                    #ek_common.copyfile(absSrcPath_Img, os.path.join(dstReworkTruckPath, fileNameJpg))
                    #ek_common.copyfile(absSrcPath_Img, dstReworkTruckPath + fileNameJpg) 
#                    absReworPath_TruckImg = os.path.join(absoluteReworkTruckPath, relPath, fileNameJpg)
#                    absMergePath_Xml = os.path.join(dstMergePath, relPath, fileNameXml)
#                    ek_common.copyfile(absSrcPath_Img, absMergePath_Img) 
#                    ek_common.copyfile(absSrcPath_Xml, absMergePath_Xml)                
#
#
#                if wantWidth <= 0 or wantHeight <= 0:
#                    continue
#                absResizePath = os.path.join(dstResizePath, relPath)                 
#                absResizePath_Img = os.path.join(absResizePath, fileNameJpg)                 
#                absResizePath_Xml = os.path.join(absResizePath, fileNameXml)           
##                print("absResizePath={0}".format(absResizePath))
##                print("absResizePath_Img={0}".format(absResizePath_Img))
##                print("absResizePath_Xml={0}".format(absResizePath_Xml))
#                ek_common.mkdir(absResizePath)  
##               print("absoluteMergeFilePath_xml=",absoluteMergeFilePath_xml)
#                ek_opencvlib.write_img(img, absResizePath_Img, wantWidth, wantHeight)
#                modify_xml.modifyAll(absMergePath_Xml,
#                                     absResizePath_Xml,
#                                     absResizePath_Img,
#                                     width,
#                                     height,
#                                     wantWidth,
#                                     wantHeight) 

def main():        
    #print("transferExtFromJPGTojpg= argv:%s argv:%s".format(sys.argv[1],sys.argv[2],sys.argv[3])) 
    transferJPGTojpg(sys.argv[1],
                     sys.argv[2],
                     sys.argv[3],
                     int(sys.argv[4]),
                     int(sys.argv[5]))    
    
if __name__ == "__main__":
    main()
