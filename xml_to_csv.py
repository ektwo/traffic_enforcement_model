import os
import sys
import glob
import pandas as pd
import xml.etree.ElementTree as ET


def convert(xmlDir, csvFilePath):
    xml_list = []
    for xml_file in glob.glob(xmlDir + '/*.xml'):
        tree = ET.parse(xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            value = (root.find('filename').text,
                     int(root.find('size')[0].text),
                     int(root.find('size')[1].text),
                     member[0].text,
                     int(member[4][0].text),
                     int(member[4][1].text),
                     int(member[4][2].text),
                     int(member[4][3].text)
                     )
            xml_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)
    
    xml_df.to_csv(csvFilePath, index=None)
    
    #return xml_df
#
#def train():
#    image_path = os.path.join(os.getcwd(), 'data', 'tf_wider_train', 'annotations','xmls')
#    xml_df = xml_to_csv(image_path)
#    labels_path = os.path.join(os.getcwd(), 'data', 'tf_wider_train','train.csv')
#    xml_df.to_csv(labels_path, index=None)
#    print('> tf_wider_train - Successfully converted xml to csv.')
#
#def val():
#    image_path = os.path.join(os.getcwd(), 'data', 'tf_wider_val', 'annotations','xmls')
#    xml_df = xml_to_csv(image_path)
#    labels_path = os.path.join(os.getcwd(), 'data', 'tf_wider_val', 'val.csv')
#    xml_df.to_csv(labels_path, index=None)
#    print('> tf_wider_val -  Successfully converted xml to csv.')


def main():
    convert(sys.argv[1], sys.argv[2])
    convert(sys.argv[3], sys.argv[4])
    #xml_df.to_csv(sys.argv[2], index=None)
    #print("sys.argv[4]=",sys.argv[4])
    #xml_df = xml_to_csv(sys.argv[3])
    #xml_df.to_csv(sys.argv[4], index=None)    

#main()
if __name__ == "__main__":
    main()        
